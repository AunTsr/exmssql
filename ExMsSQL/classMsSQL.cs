﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace ExMsSQL
{
    public class classMsSQL
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Con { get; set; }
        SqlConnection mySql;

        enum ConnectionStatus
        {
            Connected,
            NotConnect
        }

        public classMsSQL()
        {

        }

        public classMsSQL(string Server, string Database, string User, string Password)
        {
            this.ServerName = Server;
            this.DatabaseName = Database;
            this.UserName = User;
            this.Password = Password;
            this.Con = $"Data Source = {Server};Initial Catalog = {Database}; User ID = {User}; Password = {Password}";
        }

        public bool Connect()
        {
            try
            {
                mySql = new SqlConnection(this.Con);
                mySql.Open();
                mySql.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DisConnect()
        {
            try
            {
                mySql.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public DataTable CallStore(string Store, List<string> Params)
        {
            string Cmd = $"Exec {Store} (";

            for (int i = 0; i < 20; i++)
            {
                if (Params.Count - 1 >= i) Cmd = String.Concat(Cmd, "\'" + Params[i] + "\'" + ",");
                else Cmd = String.Concat(Cmd, "\'" + "" + "\'" + ",");
            }

            Cmd = Cmd.Substring(0, Cmd.Length - 1) + ");";

            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataAdapter Val = new SqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectDataAll(string Table)
        {
            string Cmd = $"Select * from {Table};";
            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataAdapter Val = new SqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectData(string Query)
        {
            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Query, mySql);
                SqlDataAdapter Val = new SqlDataAdapter(Query, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public DataTable SelectData(string Table, List<string> ColName)
        {
            string CmdBegin = "Select";
            string CmdEnd = $"From {Table}";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, ColName[i] + ",");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + CmdEnd + ";";

            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataAdapter Val = new SqlDataAdapter(Cmd, mySql);
                DataTable dt = new DataTable();
                Val.Fill(dt);
                mySql.Close();
                Command.Dispose();
                Val.Dispose();
                return dt;
            }
            catch
            {
                return new DataTable();
            }
        }

        public bool InsertData(string Table, List<string> ColName, List<string> Value)
        {
            string CmdBegin = $"Insert Into {Table} (";
            string CmdEnd = $"Values (";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, ColName[i].ToString() + ",");
                CmdEnd = String.Concat(CmdEnd, "\'" + Value[i].ToString() + "\'" + ",");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + ")" + CmdEnd.Substring(0, CmdEnd.Length - 1) + ");";

            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool UpdateData(string Table, List<string> ColName, List<string> Value, string Where)
        {
            string CmdBegin = $"UPDATE  {Table} SET (";
            string CmdEnd = $"WHERE {Where}";
            for (int i = 0; i < ColName.Count; i++)
            {
                CmdBegin = String.Concat(CmdBegin, "`" + ColName[i] + "`" + "=" + "\"" + Value[i] + "\"");
            }

            string Cmd = CmdBegin.Substring(0, CmdBegin.Length - 1) + ")" + CmdEnd + ";";

            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteData(string Table, string Where)
        {
            string Cmd = $"DELETE FROM {Table} WHERE {Where};";

            try
            {
                mySql.Open();
                SqlCommand Command = new SqlCommand(Cmd, mySql);
                SqlDataReader MyReader;
                MyReader = Command.ExecuteReader();
                while (MyReader.Read())
                {

                }
                mySql.Close();
                Command.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
